package xJanelas;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import xFuncoes.BancoDeDados;
import xJanelas.Menu;

public class Login {

	public Login() {
		
	}
	
	private JFrame janela = new JFrame();
	private JLabel imagemLogo = new JLabel();
	private JLabel labelCpf = new JLabel("CPF");
	private JTextField campoCpf = new JTextField();
	private JLabel labelSenha = new JLabel("SENHA");
	private JPasswordField campoSenha = new JPasswordField();
	private JButton botaoEntrar = new JButton("ENTRAR");
	private LineBorder borda = new LineBorder(Color.BLACK);
	
	public void desenhaTela()
	{
		imagemLogo.setSize(1280, 200);
		imagemLogo.setLocation(30, 50);
		imagemLogo.setIcon(new ImageIcon("src/xJanelas/logo.png"));
		
		janela.setBounds(0, 0, 1280, 720);
		janela.setResizable(false);
		janela.setLocationRelativeTo(null);
		janela.setLayout(null);
		janela.setTitle("Realize seu Login");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		labelCpf.setSize(50, 20);
		labelCpf.setLocation(490, 280);
		
		campoCpf.setSize(300, 30);
		campoCpf.setLocation(490, 300);
		campoCpf.setBorder(borda);
		
		labelSenha.setSize(50, 20);
		labelSenha.setLocation(490, 330);
		
		campoSenha.setSize(300, 30);
		campoSenha.setLocation(490, 350);
		campoSenha.setBorder(borda);
		
		botaoEntrar.setSize(150, 30);
		botaoEntrar.setLocation(565, 400);
		botaoEntrar.setBorder(borda);
		botaoEntrar.addActionListener
		(
			new ActionListener() 
			{
				public void actionPerformed(ActionEvent e) 
				{
					try {
						entrar();
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
				}
			}
		);
		
		janela.add(labelSenha);
		janela.add(labelCpf);
		janela.add(imagemLogo);
		janela.add(botaoEntrar);
		janela.add(campoSenha);
		janela.add(campoCpf);
		janela.setVisible(true);
	}
	
	@SuppressWarnings("static-access")
	public void entrar() throws SQLException
	{
		String cpf = campoCpf.getText();
		@SuppressWarnings("deprecation")
		String senha = campoSenha.getText();
		
		if(verificarConteudoDoCampo(cpf,senha) == true)
		{
				BancoDeDados validarBancoDeDados = new BancoDeDados(cpf, senha);
				if(validarBancoDeDados.validarAcesso() == true)
				{
					chamarJanelaMenu();
				}
		}
		
	}
	
	private boolean verificarConteudoDoCampo(String cpf, String senha)
	{
		boolean retorno = false;
		
		if(cpf.trim().equals("") || senha.trim().equals(""))
		{
			avisoDeCampoVazio();
		}else {
			retorno = true;
		}
		
		return retorno;
	}
	
	private void avisoDeCampoVazio()
	{
		JDialog senhacpfvazios = new JDialog(janela, "CPF OU SENHA VAZIOS");
		JLabel texto =  new JLabel("Preencha os campos vazios!");
		texto.setSize(100, 50);
		texto.setForeground(Color.RED);
		senhacpfvazios.add(texto);
		senhacpfvazios.setSize(300, 150);
		senhacpfvazios.setLocationRelativeTo(null);
		senhacpfvazios.setVisible(true);
	}

	private void chamarJanelaMenu()
	{
		janela.setVisible(false);
		Menu menu =  new Menu();
		menu.desenhaTela();
	}
	
}