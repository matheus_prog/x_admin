package xJanelas;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Menu {

	//Janela
	private JFrame janela = new JFrame();
	
	//Componentes do Menu
	private JPanel painelMenu = new JPanel();
	private JButton menuMetas = new JButton("Metas");
	private JButton menuChamados = new JButton("Chamados");
	private JButton menuAvisos = new JButton("Avisos");
	private JButton menuMensagens = new JButton("Mensagens");
	private JButton menuPerfil = new JButton("Perfil");
	
	//Utilitarios visuais
	private LineBorder bordaEmLinhaPreta = new LineBorder(Color.GRAY);
	
	//Painel Central
	private JPanel funcionalidades = new JPanel();
	
	//Componentes Avisos
	private DefaultListModel listModel = new DefaultListModel();
	private JList listaDeUsuarios = new JList(listModel);
	
	public Menu() {
		
	}

	public void desenhaTela()
	{
		janela.setBounds(0, 0, 1280, 720);
		janela.setResizable(false);
		janela.setLocationRelativeTo(null);
		janela.setLayout(null);
		janela.setTitle("Menu");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		painelMenu.setSize(1280, 40);
		painelMenu.setLocation(0, 0);
		painelMenu.setBorder(bordaEmLinhaPreta);
		painelMenu.setForeground(Color.GRAY);
		
		menuMetas.setBounds(20, 100, 100, 20);
		menuChamados.setBounds(140, 100, 100, 20);
		menuAvisos.setBounds(260, 100, 100, 20);
		menuMensagens.setBounds(380, 100, 100, 20);
		menuPerfil.setBounds(500, 100, 100, 20);
		
		menuMetas.addActionListener
		(
				new ActionListener() 
				{
					public void actionPerformed(ActionEvent e) 
					{
						System.out.println("funfa");
					}
				}
		);
		
		menuChamados.addActionListener
		(
				new ActionListener() 
				{
					public void actionPerformed(ActionEvent e) 
					{
						System.out.println("funfa");
					}
				}
		);
		
		menuAvisos.addActionListener
		(
				new ActionListener() 
				{
					public void actionPerformed(ActionEvent e) 
					{
						System.out.println("funfa");
					}
				}
		);
		
		menuMensagens.addActionListener
		(
				new ActionListener() 
				{
					public void actionPerformed(ActionEvent e) 
					{
						System.out.println("funfa");
					}
				}
		);
		
		menuPerfil.addActionListener
		(
				new ActionListener() 
				{
					public void actionPerformed(ActionEvent e) 
					{
						System.out.println("funfa");
					}
				}
		);
		
		painelMenu.add(menuMetas);
		painelMenu.add(menuChamados);
		painelMenu.add(menuAvisos);
		painelMenu.add(menuMensagens);
		painelMenu.add(menuPerfil);
		janela.add(painelMenu);
		janela.setVisible(true);
	}

	
}