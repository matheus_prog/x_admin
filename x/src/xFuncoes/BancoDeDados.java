package xFuncoes;

import java.sql.*;

public class BancoDeDados {

	static Connection conector;
	static PreparedStatement comandoPreparado;
	static ResultSet resultadoConsulta;
	
	static String cpf;
	static String senha;
	static String senhaDoBanco;
	
	@SuppressWarnings("static-access")
	public BancoDeDados(String cpf, String senha) 
	{
		this.cpf = cpf;
		this.senha = senha;
	}
	
	public static boolean validarAcesso() throws SQLException
	{
		boolean retorno = false;
		
		conector = DriverManager.getConnection("jdbc:mysql://localhost:3306" , "root", "");
		
		conector.setCatalog("mc");
		
		String atalho = "'" + cpf + "'";
		
		String comandoSQL = "SELECT * FROM usuarios WHERE usuarios.cpf like " + atalho + ";";
		
		comandoPreparado = conector.prepareStatement(comandoSQL);
		
		resultadoConsulta = comandoPreparado.executeQuery();
		
		while(resultadoConsulta.next())
		{
			senhaDoBanco = resultadoConsulta.getString("senha");
		}
		
		if(senha.equals(senhaDoBanco))
		{
			retorno = true;
		}
		
		return retorno;
	
	}

	public static void logar() throws SQLException
	{
		validarAcesso();
	}
}
